package main

import "fmt"

func main() {
	fruits := []string{"apple", "banana", "orange"}
	fruits = append(fruits, "kiwi", "grape")
	fmt.Println(fruits,
		"len:", len(fruits),
		"cap:", cap(fruits),
	)
	vegetables := make([]string, 0, 5)
	vegetables = append(vegetables, "carrot", "spinach")
	vegetables = append(vegetables, "leek")
	fmt.Println(vegetables,
		"len:", len(vegetables),
		"cap:", cap(vegetables),
	)
}
